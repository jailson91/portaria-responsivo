module.exports = {
    portarias: [
        { 
            titulo: "Ato nº 199, 22 de maio de 2020", 
            descricao: "Dispõe acerca do regime de trabalho diferenciado e dos prazos processuais no âmbito do Tribunal Regional Federal da 5ª Região e das Seções Judiciárias vinculadas", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Ato199.pdf" 
        },{ 
            titulo: "Portaria nº 79, 22 de maio de 2020", 
            descricao: "Prorroga o prazo de vigência das Resoluções CNJ nº 313/2020, nº 314/2020 e nº 318/2020", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Portaria79.pdf" 
        },{ 
            titulo: "Edital nº 9, 20 de maio de 2020", 
            descricao: "INSPEÇÕES JUDICIAIS 2020 - ALTERAÇÃO", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Edital092020DF.pdf" 
        },{ 
            titulo: "Recomendação nº 66, 13 de maio de 2020", 
            descricao: "Recomenda aos Juízos com competência para o julgamento das ações que versem sobre o direito à saúde a adoção de medidas para garantir os melhores resultados à sociedade durante o período excepcional de pandemia da Covid-19.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/RecomendaccaoCNJ66.pdf" 
        },{ 
            titulo: "Resolução nº 318, 7 de maio de 2020", 
            descricao: "Prorroga, no âmbito do Poder Judiciário, em parte, o regime instituído pelas Resoluções nº 313, de 19 de março de 2020, e nº 314, de 20 de abril de 2020, e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Resolucao318.pdf" 
        },{ 
            titulo: "Ato nº 162, 7 de maio de 2020", 
            descricao: "Dispõe acerca do regime de trabalho diferenciado e dos prazos processuais no âmbito do Tribunal Regional Federal da 5ª Região e das Seções Judiciárias vinculadas.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Ato-Coronavirus-sexto.pdf" 
        },{ 
            titulo: "Portaria nº 251, 28 de abril de 2020", 
            descricao: "Regulamenta as atividades a serem cumpridas pelos servidores em regime de trabalho diferenciado cujas atribuições regulares são predominantemente presenciais.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/PORTARIA-N-251-2020.pdf" 
        },{ 
            titulo: "Ato nº 140, 21 de abril de 2020", 
            descricao: "Dispõe acerca do regime de trabalho diferenciado e dos prazos processuais no âmbito do Tribunal Regional Federal da 5ª Região e das Seções Judiciárias vinculadas", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Ato140daPresidenciaResoluc%CC%A7a%CC%83o314CNJ.pdf" 
        },{ 
            titulo: "Resolução nº 314, 20 de abril de 2020", 
            descricao: "Prorroga, no âmbito do Poder Judiciário, em parte, o regime instituído pela Resolução nº 313, de 19 de março de 2020, modifica as regras de suspensão de prazos processuais e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Resolucao314.pdf" 
        },{ 
            titulo: "Portaria nº 180, 31 de março de 2020", 
            descricao: "Dispõe sobre a prorrogação do regime de teletrabalho no Tribunal Regional Federal da 5ª Região e Seções Judiciárias vinculadas", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Portaria-180-Prorrogacao-teletrabalho.pdf" 
        },{ 
            titulo: "Ato Conjunto TRF-5, 23 de março de 2020", 
            descricao: "Dispõe sobre a destinação de pena de prestação pecuniária, transação penal e suspensão condicional do processo nas ações criminais para o enfrentamento da pandemia decorrente do novo coronavírus – COVID-19", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/ATO_CONJUNTO1_23DEMARCODE2020.pdf" 
        },{ 
            titulo: "Portaria nº 57, 20 de março de 2020", 
            descricao: "Incluir no Observatório Nacional sobre Questões Ambientais, Econômicas e Sociais de Alta Complexidade e Grande Impacto e Repercussão o caso Coronavirus - Covid-19", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/PORTARIA_57.pdf" 
        },{ 
            titulo: "Resolução nº 313, de 19 de março de 2020", 
            descricao: "Estabelece, no âmbito do Poder Judiciário, regime de Plantão Extraordinário, para uniformizar o funcionamento dos serviços judiciários, com o objetivo de prevenir o contágio pelo novo Coronavírus – Covid-19, e garantir o acesso à justiça neste período emergencial.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Resolucao-CNJ-Coronavirus.pdf" 
        },{ 
            titulo: "Ato nº 112, de 19 de março de 2020", 
            descricao: "Dispõe sobre medidas de prevenção relativas ao COVID-19.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Ato-112-TRF5-Coronavirus.pdf" 
        },{ 
            titulo: "Portaria Conjunta nº 1, de 18 de março de 2020", 
            descricao: "Dispõe sobre a padronização de procedimentos e atividades dos Analistas Judiciários - Área Judiciária - Especialidade Execução de Mandados na prevenção ao COVID-19.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Portaria_Conjunta_01_2020.pdf" 
        },{ 
            titulo: "Recomendação Nº 62, de 17 de março de 2020", 
            descricao: "Recomenda aos Tribunais e magistrados das preventivas à propagação da infecção pelo novo coronavírus – Covid-19 no âmbito dos sistemas de justiça penal e socioeducativo.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/62_Recomendacao.pdf" 
        },{ 
            titulo: "Portaria nº 167, de 17 de março de 2020", 
            descricao: "Dispõe sobre medidas de prevenção relativas ao COVID-19 na Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Portaria-JFPB-Coronavirus.pdf" 
        },{ 
            titulo: "Ato nº 101, de 12 de março de 2020", 
            descricao: "Dispõe sobre medidas de prevenção relativas ao COVID-19.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Ato-TRF5-Coronavirus.pdf" 
        },{ 
            titulo: "Portaria nº 14, de 27 de janeiro de 2020", 
            descricao: "Correição Ordinária na Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/Portaria-014-2020-TRF5.pdf" 
        },{ 
            titulo: "Portaria nº 1, de 2 de janeiro de 2020", 
            descricao: "Estabelece regras acerca do atendimento realizado pelas unidades integrantes da Seção Judiciária da Paraíba e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2020/P12020.pdf" 
        },{ 
            titulo: "Ato nº 04, de 09 de janeiro de 2019", 
            descricao: "Torna Público os feriados, no âmbito do Tribunal Regional Federal Federal da 5ª Região, durante o exercício de 2019.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/Feriados-TRF-2019.pdf" 
        },{ 
            titulo: "Portaria nº 61, de 25 de janeiro de 2019", 
            descricao: "Restabelece a tramitação dos pedidos de desarquivamento de autos judiciais.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P61-25012019.pdf" 
        },{ 
            titulo: "Portaria nº 132, de 14 de fevereiro de 2019", 
            descricao: "Decreta como 'ponto facultativo' o dia 06 de março de 2019 (quarta-feira de cinzas), no âmbito da Seção Judiciária da Paraíba, e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P132-14022019.pdf" 
        },{ 
            titulo: "Portaria nº 189, de 13 de março de 2019", 
            descricao: "Institui o Escritório de Inovação, com a finalidade de desenvolver atividades de pesquisa e implementar soluções relacionadas às atividades da Justiça Federal na Paraíba, em especial na área de Tecnologia da Informação.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P189-13032019.pdf" 
        },{ 
            titulo: "Portaria nº 244, de 22 de março de 2019", 
            descricao: "Estabelece critérios para a distribuição e a migração de ações recebidas em meio físico, decorrentes de declinação de competência, no âmbito da Sede da Seção Judiciária.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P244-22032019.pdf" 
        },{ 
            titulo: "Portaria nº 274, de 01 de abril de 2019", 
            descricao: "Estabelece critérios para a tramitação das ações nos Juizados Especiais Federais e da Turma Recursal, na Sede da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P274-01042019.pdf" 
        },{ 
            titulo: "Portaria nº 279, de 03 de abril de 2019", 
            descricao: "Estabelece regras mínimas de segurança, no âmbito da Seção Judiciária da Paraíba, institui o Grupo de Segurança Operacional e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P279-03042019.pdf" 
        },{ 
            titulo: "Ordem de Serviço nº 0926683, de 22 de abril de 2019", 
            descricao: "Estabelece critérios para a distribuição de processos oriundos de outros Órgãos Judiciais, de acordo com a Resolução 10/2016 do TRF da 5ª Região.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/os0926683.pdf" 
        },{ 
            titulo: "Edital nº 7, de 09 de maio de 2019", 
            descricao: "Faz saber aos que lerem o presente Edital ou dele tiverem conhecimento que, em 2019, serão realizadas INSPEÇÕES JUDICIAIS nas Varas Federais da Seção Judiciária da Paraíba, nos períodos neste instrumento especificados, sob a presidência dos respectivos Juízes Federais, com o auxílio dos Juízes Federais Substitutos, onde houver.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/ED07-09052019.pdf" 
        },{ 
            titulo: "Portaria nº 429, de 17 de maio de 2019", 
            descricao: "Disciplina o funcionamento da Seção de Arquivo e Depósito Judicial, relativamente ao arquivamento e desarquivamento de autos, na Sede desta Seccional, e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P429-17052019.pdf" 
        },{ 
            titulo: "Portaria nº 436, de 21 de maio de 2019", 
            descricao: "Transfere o ponto facultativo do dia 20 (Corpus Christi) para o dia 21 de junho 2019, no âmbito da Seção Judiciária da Paraíba, e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P436-21052019.pdf" 
        },{ 
            titulo: "Ato nº 208, de 04 de junho de 2019", 
            descricao: "Regulamenta o Sistema Eletrônico de Execução Unificado (SEEU) no âmbito do Tribunal Regional Federal da 5ª Região e Seções Judiciárias vinculadas.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/ato208-04062019.pdf" 
        },{ 
            titulo: "Portaria nª 34, de 13 de agosto de 2019", 
            descricao: "Implanta a CENTRAL DE MANDADOS no âmbito da Subseção Judiciária de Campina Grande e aprova seu REGULAMENTO.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/Portaria34-2019.pdf" 
        },{ 
            titulo: "Resolução Pleno Nº 08, de 04 de setembro de 2019", 
            descricao: "Disciplina a tramitação dos inquéritos, ações penais e procedimentos criminais incidentais no PJe", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/R08-2019.pdf" 
        },{ 
            titulo: "Portaria da Direção do Foro nº 868 de 27 de Setembro de 2019", 
            descricao: "Revoga a Portaria/GDF, nº 189/2019, que instituiu o ESCRITÓRIO DE INOVAÇÃO da Seção Judiciária da Paraíba e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/Portaria_n__868_GDF_2019.pdf" 
        },{ 
            titulo: "Ato nº 386, de 03 de Outubro de 2019", 
            descricao: "Transfere para 31 de outubro de 2019, quinta-feira, as comemorações alusivas ao Dia do Servidor Público, previsto no art. 236, da Lei nº 8.112/90", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/Ato-386-2019-TRF5.pdf" 
        },{ 
            titulo: "Ato nº 480, de 13 de Dezembro de 2019", 
            descricao: "Lista as comarcas estaduais que permanecem com a competência federal delegada para processamento e julgamento de causas de natureza previdenciária, conforme o disposto no inc. III, do art. 15, da Lei 5.010, de 30 de maio de 1966, com a redação dada pelo art. 3º, da Lei 13.876, de 20 de setembro de 2019.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/A480-2019.pdf" 
        },{ 
            titulo: "Portaria nº 1129, de 13 de Dezembro de 2019", 
            descricao: "Institui o Plantão Regionalizado de 2020.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/P1129-2019.pdf" 
        },{ 
            titulo: "Resolução nº 614, de 16 de Dezembro de 2019", 
            descricao: "Dispõe sobre a alteração dos art. 14, 15 e 16 da Resolução CJF n. 318 de 4 de novembro de 2014", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/Resolucao6142019CNJ.pdf" 
        },{ 
            titulo: "Ato nº 507, de 26 de Dezembro de 2019", 
            descricao: "Torna públicos os feriados, no âmbito do Tribunal Regional Federal da 5ª Região, durante o exercício de 2020", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2019/ATO_507_26_DEZ_2019_FERIADOS_2020.pdf" 
        },{ 
            titulo: "Portaria nº 54, de 01 de fevereiro de 2018", 
            descricao: "Institui a Central Integrada de Mandados para o cumprimento de diligências entre a Sede da Seção e as Subseções Judiciárias da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/Portaria%20Central%20Integrada%20de%20Mandados%20-%20Pub..pdf" 
        },{ 
            titulo: "Portaria nº 137, de 14 de março de 2018", 
            descricao: "Institui medidas visando à redução de despesas, no âmbito da Seção Judiciária da Paraíba, e dá outras proviências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/portaria-137.pdf" 
        },{ 
            titulo: "Portaria nº 158, de 14 de março de 2018", 
            descricao: "Apresenta esclarecimentos sobre permutas entre oficiais de justiça dos Juizados Especiais Federais, da Turma Recursal e oficiais de justiça das varas comuns, e dá outras providências", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/portaria-158.pdf" 
        },{ 
            titulo: "Resolução Pleno nº 03, de 21 de março de 2018", 
            descricao: "Dispõe sobre a digitalização de processos físicos em tramitação e sua inclusão no sistema de Processo Judicial Eletrônico - PJe", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/RP03-21032018.pdf" 
        },{ 
            titulo: "Portaria nº 189, de 22 de março de 2018", 
            descricao: "Suspensão de prazo processual por falta de energia elétrica", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/portaria-189.pdf" 
        },{ 
            titulo: "Ato nº 127, de 25 de abril de 2018", 
            descricao: "Dispõe sobre o funcionamento da Justiça Federal de 1º e 2º Graus da 5ª Região, durante os jogos do Brasil na Copa do Mundo de 2018, e estabelece outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/ato-127-trf5.pdf" 
        },{ 
            titulo: "Portaria nº 292, de 27 de abril de 2018", 
            descricao: "Institui condecorações a serem outorgadas pela Justiça Federal, Seção Judiciária da Paraíba, e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/27042018-P292GDF.pdf" 
        },{ 
            titulo: "Portaria nº 294, de 30 de abril de 2018", 
            descricao: "Institui Comissão para digitalização de processos físicos em tramitação na Sede da Seção Judiciária da Paraíba e inclusão no sistema de Processo Judicial Eletrônico - Pje.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/portaria-294.pdf" 
        },{ 
            titulo: "Portaria nº 296, de 02 de maio de 2018", 
            descricao: "Dispõe sobre a concessão de auxílio-transporte aos servidores do Quadro de Pessoal da Justiça Federal na Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/02052018-P296GDF.pdf" 
        },{ 
            titulo: "Portaria nº 10, de 24 de maio de 2018", 
            descricao: "Suspende o expediente forense e prorroga os prazos com vencimento em 24 e 25/05/2018.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/P10GDF-24052018.pdf" 
        },{ 
            titulo: "Portaria nº 368, de 27 de maio de 2018", 
            descricao: "Prorroga os prazos processuais com vencimento no dias 28 maio 2018 para o primeiro dia útil subsequente na Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/P368-27052018.pdf" 
        },{ 
            titulo: "Resolução Pleno 09, de 27 de junho de 2018", 
            descricao: "Regulamenta, no âmbito da Justiça Federal de 1º e 2º graus da 5ª Região, a expedição de certidões judiciais e dá outras providências", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/RP09-27062018.pdf" 
        },{ 
            titulo: "Ato nº 301, de 29 de agosto de 2018", 
            descricao: "Dispõe sobre a obrigatoriedade de utilização do PJe para impetração de Mandado de Segurança e Interposição de Agravo de Instrumento.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/A301-29082018.pdf" 
        },{ 
            titulo: "Portaria nº 759, de 15 de outubro de 2018", 
            descricao: "Estabelece critérios para a distribuição de ações em meio físico, decorrentes de declinação de competência, perante os Juizados Especiais Federais na sede da Seção Judiciária.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/P759-15102018.pdf" 
        },{ 
            titulo: "Edital nº 11, de 23 de outubro de 2018", 
            descricao: "Editais de Inspeções Judiciais 2019", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/EDITAL11-INSPECOES2019.pdf" 
        },{ 
            titulo: "Resolução Pleno nº 18, de 12 de dezembro de 2018", 
            descricao: "Amplia a competência das 1ª, 2ª e 3ª Varas Federais da sede da Seção Judiciária da Paraíba - SJPB.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/Alteracao-Competencia-das-Varas-Civeis.pdf" 
        },{ 
            titulo: "Portaria nº 958, de 13 de dezembro de 2018", 
            descricao: "Institui o Plantão Anual Regionalizado durante o ano de 2019, na Sede da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/P958-13122018.pdf" 
        },{ 
            titulo: "Portaria nº 979, de 17 de dezembro de 2018", 
            descricao: "Restringe a tramitação dos pedidos de desarquivamento de autos judiciais.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2018/P979-17122018.pdf" 
        },{ 
            titulo: "Portaria nº 007/GDF, de 10 de Março de 2017", 
            descricao: "Dispõe sobre a adoção de lista de verificação (checklists) nos processos administrativos, com vista ao aperfeiçoamento da Gestão de Riscos e das estruturas de Controle Interno da SJPB.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/10032017-P007GDF.pdf" 
        },{ 
            titulo: "Portaria nº 010/GDF, de 21 de Março de 2017", 
            descricao: "Autoriza o TELETRABALHO em regime parcial e experimental, no âmbito da Secretaria Administrativa.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/21032017-P010GDF.pdf" 
        },{ 
            titulo: "Portaria nº 011/GDF, de 27 de Março de 2017", 
            descricao: "Revoga a Portaria nº 116/GDF de 15 de Setembro de 2017 e estabelece que, para a concessão de diárias para os Oficiais de Justiça deve ser observada integralmente a Resolução nº CJF-RES-2015/00340 de 11/02/2015, que dispõe sobre a regulamentação e concessão de diárias e da aquisição de passagens áreas no âmbito do CJF de Primeiro e Segundo grau.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/27032017-P011GDF.pdf" 
        },{ 
            titulo: "Portaria nº 014/GDF, de 29 de Março de 2017", 
            descricao: "Altera a Portaria nº123/GDF de 08 de outubro de 2015 que estabeleceu o Regulamento da Central de Mandados/CEMAN da JFPB.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/29032017-P014GDF.pdf" 
        },{ 
            titulo: "Portaria nº 01/2017-CEJUSC, de 09 de Maio de 2017", 
            descricao: "Estabelece a utilização do SISTEMA DE MARCAÇÃO DE AUDIÊNCIAS pelas Varas Federais Comuns e pela Vara Federal Privativa de Execuções Fiscais.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/09052017-P01CEJUSC.pdf" 
        },{ 
            titulo: "Ato nº 242/2017, de 25 de Abril de 2017", 
            descricao: "Cria o sistema de convênios SCC.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/ATO_Cria_o_Sistema_de_Convenios_SCC.pdf" 
        },{ 
            titulo: "Portaria nº 020/GDF, de 28 de Abril de 2017", 
            descricao: "Delega à Direção da Secretaria Administrativa e à Direção do Núcleo de Gestão de Pessoas competência para a prática de atos administrativos.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/28042017-P020GDF.pdf" 
        },{ 
            titulo: "Ordem de Serviço nº 0060562/GDF, de 09 de Junho de 2017", 
            descricao: "Estabelece rotinas de trabalho direcionadas à Seções de Distribuição (varas comuns), na hipótese de interposição de recursos, ações conexas e incidentes processuais interpostos em autos físicos de natureza penal.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/09072017-OS0060562GDFDF.pdf" 
        },{ 
            titulo: "Portaria nº 1162/GDF, de 08 de Agosto de 2017", 
            descricao: "Estabelece critérios para a implantação e operacionalização, pelas Varas Federais e CEJUSC, da comunicação dos atos processuais (intimações) através da ferramenta WhatsApp na Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/08082017-P1162GDF.pdf" 
        },{ 
            titulo: "Portaria nº 112/GDF, de 25 de Agosto de 2017", 
            descricao: "Dispõe sobre a apresentação e tramitação de Reclamações Pré-Processuais no “Ambiente do Centro de Conciliação”, no Sistema de Processo Judicial Eletrônico – PJe, no âmbito da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/25082017-P112GDF.pdf" 
        },{ 
            titulo: "Portaria nº 1003/GDF, de 18 de Setembro de 2017", 
            descricao: "Regulamenta a destinação das armas de fogo apreendidas e dos bens vinculados a Ações Penais e Inquéritos Policiais arquivados e desaforados por declinação de competência e institui a lista de verificação para baixa definitiva/incompetência de feitos de natureza criminal", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/18092017-P1003GDF.pdf" 
        },{ 
            titulo: "Portaria nº 1448/GDF, de 27 de Outubro de 2017", 
            descricao: "Altera a Portaria nº 020/GDF, de 28/abril/2017, que delega à Direção da Secretaria Administrativa e à Direção do Núcleo de Gestão de Pessoas a competência para a prática de atos administrativos", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/Di%C3%A1rio%20Eletr%C3%B4nico%20Administrativo%20SJPB%20203.02017.pdf" 
        },{ 
            titulo: "Portaria nº 1448/GDF, de 27 de Outubro de 2017", 
            descricao: "Altera a Portaria nº 020/GDF, de 28/abril/2017, que delega à Direção da Secretaria Administrativa e à Direção do Núcleo de Gestão de Pessoas a competência para a prática de atos administrativos", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/Di%C3%A1rio%20Eletr%C3%B4nico%20Administrativo%20SJPB%20203.02017.pdf" 
        },{ 
            titulo: "Portaria nº 1450, de 06 de Novembro de 2017", 
            descricao: "Disciplina os serviços administrativos referentes ao Plantão Judiciário na Seção Judiciária da Paraíba e dá outras providências", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/1450.pdf" 
        },{ 
            titulo: "Portaria nº 1460, de 06 de Novembro de 2017", 
            descricao: "Institui o Centro de Inteligência na Seção Judiciaria da Paraíba e dá outras providências", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/1460.pdf" 
        },{ 
            titulo: "Ato nº 572 2017", 
            descricao: "Relativo aos feriados e publi...", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/ATO%20N%C2%BA%20572%202017%20RELATIVO%20AOS%20FERIADOS%20E%20PUBLICA%C3%87%C3%83O.pdf" 
        },{ 
            titulo: "Portaria nº 1544, de 04 de Dezembro de 2017", 
            descricao: "Disciplina prazos para procedimentos licitatórios", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2017/Portaria1544_2017.pdf" 
        },{ 
            titulo: "Portaria nº 018/GDF, de 07 de Março de 2016", 
            descricao: "Determina a atualização do PJe quanto a tramitação das Cartas Precatórias Fiscais, em decorrências da associação do assunto “Direito Processual Civil do Trabalho / Objetos de Cartas Precatórias / De Ordem / Atos Executórios” à competência Execução Fiscal, assim como em face da desvinculação deste assunto propriamente dito em relação às demais competências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/07032016-P018GDF.pdf" 
        },{ 
            titulo: "Portaria nº 020/GDF, de 14 de Março de 2016", 
            descricao: "Atualiza a relação de feriados e pontos facultativos na Seção Judiciária da Paraíba, revogando a Portaria nº 002/GDF de Janeiro de 2016.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/14032016-P020GDF.pdf" 
        },{ 
            titulo: "Resolução nº 04, de 16 de Março de 2016", 
            descricao: "Audiência de Custódia - Dispõe sobre a apresentação de pessoa detida em flarante delito, até 24 horas após a sua prisão, para participar de audiência de custódia.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/RESOLUCAO_Audiencia_de_custodia.pdf" 
        },{ 
            titulo: "Resolução Pleno nº 10, de 10 de Junho de 2016", 
            descricao: "Dispõe sobre a anexação de documentos no Sistema de Processo Judicial Eletrônico Federal - PJe de 1º e 2º Graus no âmbito da 5ª Região.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/RP10-10062016.pdf" 
        },{ 
            titulo: "Portaria nº 042/GDF, de 29 de Julho de 2016", 
            descricao: "Determina a utilização obrigatória do Processo Judicial Eletrônico – PJe para o processamento e tramitação das demandas judiciais da classe EXECUÇÃO PENAL, a partir de setembro de 2016, bem como de todos os seus incidentes processuais e ações conexas, no âmbito da SJPB e sua Subseções.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/29072016-P042GDF.pdf" 
        },{ 
            titulo: "Portaria nº 043/GDF, de 29 de Julho de 2016", 
            descricao: "Institui o CEJUSC, vinculado ao Núcleo Judiciário de Secretária Administrativa desta Seção Judiciária, responsavel pelo desenvolvimento de programas destinados a auxiliar, orientar e estimular a autocomposição, através da realização de sessões e audiências de conciliação e/ou mediação.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/29072016-P043GDF.pdf" 
        },{ 
            titulo: "Portaria nº 054/GDF, de 14 de Setembro de 2016", 
            descricao: "Atualiza as normas que regulamentam o funcionamento da Biblioteca “Juiz Federal Agnelo Amorim Filho” da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/14092016-P054GDF.pdf" 
        },{ 
            titulo: "Portaria nº 056/GDF, de 28 de Setembro de 2016", 
            descricao: "Disciplina procedimentos e estabelece prazos para a marcação das férias regulamentares, no âmbito da SJPB, de acordo com a resolução nº 221/22, do Conselho da Justiça Federal.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/28092016-P056GDF.pdf" 
        },{ 
            titulo: "Portaria nº 070/GDF, de 28 de Novembro de 2016", 
            descricao: "Define normais gerais sobre a administração de material e patrimônio na SJPB.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/28112016-P070GDF.pdf" 
        },{ 
            titulo: "Portaria nº 074/GDF, de 02 de Dezembro de 2016", 
            descricao: "Determina a utilização obrigatória, a partir de Janeiro 2017, do Processo Judicial Eletrônico – PJe para o processamento e tramitação das demandas judiciais dos feitos relativos a classe EXECUÇÃO FISCAL e suas ações conexas, acidentais ou dependentes, em toda a SJPB.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/02122016-P074GDF.pdf" 
        },{ 
            titulo: "Portaria nº 076/GDF, de 09 de Dezembro de 2016", 
            descricao: "Estabelece a utilização obrigatória, a partir de Janeiro de 2017, do Processo Judicial Eletrônico – PJe para ajuizamento e tramitação das demandas judiciais no âmbito da SJPB.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/09122016-P076GDF.pdf" 
        },{ 
            titulo: "Portaria nº 079/GDF, de 16 de Dezembro de 2016", 
            descricao: "Dispõe sobre os procedimentos básicos a serem adotados no acompanhamento e fiscalização da execução dos contratos administrativos, no âmbito da Seção Judiciária da Paraíba, e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/16122016-P079GDF.pdf" 
        },{ 
            titulo: "Portaria nº 080/GDF, de 16 de Dezembro de 2016", 
            descricao: "Dispõe sobre os procedimentos para a apuração de infrações e aplicação de penalidades a licitantes e contratados (as) no âmbito da Seção Judiaria da Paraíba e outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/16122016-P080GDF.pdf" 
        },{ 
            titulo: "Portaria nº 081/GDF, de 16 de Dezembro de 2016", 
            descricao: "Dispõe sobre a obrigatoriedade de utilização do Sistema de Processo Judicial Eletrônico - Pje, no ambiente de Custódia, na Seção e Subseções Judiciárias da Paraíba e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2016/16122016-P081GDF.pdf" 
        },{ 
            titulo: "Portaria nº 050/GDF, de 07 de Abril de 2015", 
            descricao: "Revoga a Portaria nº 012/GDF de 07 de Janeiro de 2011 e dispõe sobre o horário de funcionamento da JFPB; a jornada de trabalho, o sistema eletrônico de controle de frequência, a instituição do banco de horas, a concessão de folga compensatória para os servidores lotados na Secretaria Administrativa da Sede e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2015/07042015-P050GDF.pdf" 
        },{ 
            titulo: "Portaria nº 079/GDF, de 19 de Junho de 2015", 
            descricao: "Estabelece que a concessão de diárias para os Oficiais de Justiça pressupõe decisão prévia de autoridade administrativa, conforme art 3º, II da Resolução nº 73/2009 do CNJ.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2015/19062015-P079GDF.pdf" 
        },{ 
            titulo: "Portaria nº 082/GDF, de 25 de Junho de 2015", 
            descricao: "Revoga a Portaria nº 139/GDF de 12 de fevereiro de 2003 e estabelece normas para a utilização do estacionamento da sede da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2015/25062015-P082GDF.pdf" 
        },{ 
            titulo: "Portaria nº 131/GDF, de 11 de Novembro de 2015", 
            descricao: "Revoga o capítulo V da Portaria nº 237/GDF de 23 de Março de 2006 e dispõe sobre a obrigatoriedade de devolução do cartão ou token, crachá de identificação, chave(s) de acesso a Setores deste Seção Judiciária e outros documentos e materiais sob guarda do servidor, quando do seu desligamento ou afastamento legal prolongado.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2015/11112015-P131GDF.pdf" 
        },{ 
            titulo: "Portaria nº 141/GDF, de 04 de Dezembro de 2015", 
            descricao: "Disciplina, no âmbito da Seção Judiciária da Paraíba, os procedimentos de acesso à informação que tratam a Lei no 12.527 de 18 de novembro de 2011 e o Decreto nº 7.724 de 16 de Maio de 2012.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2015/04122015-P141GDF.pdf" 
        },{ 
            titulo: "Portaria nº 006/GDF, de 14 de Janeiro de 2014", 
            descricao: "Estabelece que as ações conexas e o incidentes processuais deverão ser propostos exclusivamente pelo Processo Judicial Eletrônico, ainda que a ação principal a qual estejam vinculadas tramite por meio físico.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/14012014-P006GDF.pdf" 
        },{ 
            titulo: "Portaria nº 034/GDF, de 28 de Março de 2014", 
            descricao: "Amplia a obrigatoriedade do Processo Judicial Eletrônico – PJe para as demais classes cíveis e abrange as cartas precatórias, de ordem e rogatórias.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/28032014-P034GDF.pdf" 
        },{ 
            titulo: "Portaria nº 039/GDF, de 22 de Abril de 2014", 
            descricao: "Determina que sejam distribuídas para as Varas Comuns as cartas deprecadas pela Justiça Estadual, no exercício da competência delegada de que trata o art. 109, §3º, da Constituição Federal.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/22042014-P039GDF.pdf" 
        },{ 
            titulo: "Portaria nº 041/GDF, de 24 de Abril de 2014", 
            descricao: "Dispõe sobre a emissão de Atestado De Capacidade Técnica - ACT, no âmbito da Justiça Federal na Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/24042014-P041GDF.pdf" 
        },{ 
            titulo: "Portaria nº 045/GDF, de 29 de Abril de 2014", 
            descricao: "Dispõe sobre a concessão de estágio a estudantes de nível superior no âmbito da Seção Judiciárias da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/29042014-P045GDF.pdf" 
        },{ 
            titulo: "Portaria nº 073/GDF, de 18 de Junho de 2014", 
            descricao: "Altera, em parte, a Portaria nº 396/GDF, de 07 de Maio de 2010, que dispõe sobre a Central de Mandados da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/18062014-P073GDF.pdf" 
        },{ 
            titulo: "Portaria nº 076/GDF, de 30 de Junho de 2014", 
            descricao: "Institui a Comissão Socioambiental e regulamenta sua atribuições no âmbito da Seção Judiciária na Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/30062014-P076GDF.pdf" 
        },{ 
            titulo: "Portaria nº 083/GDF, de 30 de Julho de 2014", 
            descricao: "Dispõe sobre o procedimento administrativo de transferência de bens móveis entre os setores da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/30072014-P083GDF.pdf" 
        },{ 
            titulo: "Portaria nº 086/GDF, de 07 de Agosto de 2014", 
            descricao: "Estabelece diretrizes gerais para a redistribuição dos processos penais em tramitação na 1º, 2º e 3º Varas Federais.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/07082014-P086GDF.pdf" 
        },{ 
            titulo: "Portaria nº 149/GDF, de 14 de Novembro de 2014", 
            descricao: "Atualiza o quadro de substituições automáticas dos magistrados da Seção Judiciária da Paraíba.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2014/14112014-P149GDF.pdf" 
        },{ 
            titulo: "Portaria nº 33/GDF, de 16 de Abril de 2013", 
            descricao: "Determina que esta Seção Judiciária observe integralmente, durante todas as fases dos procedimentos licitatórios, a legislação que obriga a inserção de critérios e quesitos de sustentabilidade ambiental.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2013/16042013-P033GDF.pdf" 
        },{ 
            titulo: "Portaria nº 84/GDF, de 18 de Julho de 2013", 
            descricao: "Amplia a obrigatoriedade de utilização do Processo Judicial Eletrônico (PJe) na Sede da Seção Judiciária e nas Subseção e dá outras porvidências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2013/portaria_084.pdf" 
        },{ 
            titulo: "Portaria nº 133/GDF, de 26 de Setembro de 2013", 
            descricao: "Amplia a obrigatoriedade de utilização do Processo Judicial Eletrônico(PJe), na sede da Seção Judiciária e nas Subseções Judiciárias.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2013/portaria_133.pdf" 
        },{ 
            titulo: "Portaria nº 155/GDF, de 07 de Novembro de 2013", 
            descricao: "Regula o procedimento de averbação de tempo de serviço prestado por servidor da Seção Judiciária da Paraíba junto a instituições privadas ou a outros órgãos públicos.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2013/07112013-P155GDF.pdf" 
        },{ 
            titulo: "Portaria nº 189/GDF, de 12 de Dezembro de 2013", 
            descricao: "Estabelece que, enquanto não houver normativo específico, emitido pelo CJF e/ou TRF5, que os servidores efetivos, oriundos do serviço público dos Estados, do Distrito Federal e dos Municípios, ainda que regidos por Regime Próprio de Previdência nos órgãos originários com ingresso na Seção Judiciária da Paraíba, a partir de 14 de outubro de 2013, sem quebra de continuidade, sujeitar-se-ão ao novo regime de previdência complementar instituído pela Lei nº 12.618 de 30 de abril de 2012.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2013/12122013-P189GDF.pdf" 
        },{ 
            titulo: "Portaria nº 059/GDF, de 04 de Junho de 2012", 
            descricao: "Disciplina os procedimentos de atermação de pedidos, cadastro de advogados e serviço de Telejuizado para funcionamento junto aos Juizados Especiais Federais de João Pessoa (7ª e 13ª Varas) e Turma Recursal.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2012/04062012-P059GDF.pdf" 
        },{ 
            titulo: "Portaria nº 096/GDF, de 16 de Agosto de 2012", 
            descricao: "Disciplina a concessão de licença para capacitação prevista no art. 87 da Lei nº 8.112/90, com a redação dada pela Lei nº 9.527/97.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2012/16082012-P096GDF.pdf" 
        },{ 
            titulo: "Portaria nº 093/GDF, de 10 de Agosto de 2012", 
            descricao: "Intitui o uso do correio eletrônico como meio de comunicação geral, em substituição aos memorandos, ofícios, notificações, avisos e intimações, inclusive em processos administrativos, sendo nestes preferencialmente.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2012/Portaria%20093_%20GDF%2010_08_2012.pdf" 
        },{ 
            titulo: "Portaria nº 036/GDF, de 22 de Fevereiro de 2011", 
            descricao: "Disciplina o cumprimento das diligências externas da Seção de Arquivo e Depósito Judicial, que exijam a presença de Executantes de Mandados.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2011/22022011-P036GDF.pdf" 
        },{ 
            titulo: "Portaria nº 013/GDF, de 06 de Janeiro de 2010", 
            descricao: "Dispõe sobre a utilização do correio eletrônico como meio institucional de comunicação, diminuição do impacto ambiental das atividades internas da Seção Judiciária da Paraíba e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2010/06012010-P013GDF.pdf" 
        },{ 
            titulo: "Portaria nº 223/GDF, de 15 de Março de 2010", 
            descricao: "Determina que a Seção de Segurança e Transportes funcionará em expediente integral, devendo, para tanto permanecer pelo menos um servidor em atividade, de segunda a sexta-feira, das 7h30min às 19h.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2010/15032010-P223GDF.pdf" 
        },{ 
            titulo: "Portaria nº 357/GDF, de 30 de Abril de 2010", 
            descricao: "Constitui o “Comitê Institucional da Seção Judiciária da Paraíba – SJPB”, que será composto pelos ocupantes de cargos ou funções comissionadas.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2010/30042010-P357GDF.pdf" 
        },{ 
            titulo: "Portaria nº 03/GAB/TR, de 18 de Maio de 2010", 
            descricao: "Dispõe sobre o horário de funcionamento da Turma Recursal; a jornada de trabalho dos servidores e estagiários; o sistema eletrônico de controle de frequência; a instituição do banco de horas e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2010/18052010-P03GABTR.pdf" 
        },{ 
            titulo: "Portaria nº 012/2010-GDS de 27 de Maio de 2010", 
            descricao: "Regulamenta o funcionamento do Juizado Especial Federal Adjunto Cível da 8ª Vara Federal da Paraíba, Subseção Judiciária de Sousa e adota outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2010/27052010-P012GDS.pdf" 
        },{ 
            titulo: "Portaria nº 013/2010-GDS de 28 de Maio de 2010", 
            descricao: "Altera o regulamento de funcionamento do Juizado Especial Federal Adjunto Cível da 8ª Vara Federal da Paraíba, Subseção Judiciária de Sousa e adota outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2010/28052010-P013GDS.pdf" 
        },{ 
            titulo: "Portaria nº 09/7ª Vara, de 04 de Maio de 2009", 
            descricao: "Dispõe sobre a uniformização dos procedimentos quanto aos processos que tratam de beneficios por incapacidade no âmbito do Juízado Especial Federal da SJPB.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2009/04052009-P09GDF.pdf" 
        },{ 
            titulo: "Portaria nº 31/08ª Vara, de 06 de Outubro de 2009", 
            descricao: "Determina que as partes que já ajuizaram ações pleiteando concessão/restabelecimento do beneficio de Amparo Assistencial de que trata a Lei nº 8741/93, poderão ter seus processos julgados sem a necessidade de realização de audiência de instrução.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2009/306102009-P31GDF.pdf" 
        },{ 
            titulo: "Portaria nº 1290/GDF, de 05 de Novembro de 2009", 
            descricao: "Determina que seja efetuada escala de trabalho/horário dos servidores Técnico Judiciário - Especialidade Segurança e Transportes lotados na SA, que não exercem atividades comissionadas.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2009/05112009-P1290GDF.pdf" 
        },{ 
            titulo: "Portaria nº 138/GJF, de 24 de Julho de 2006", 
            descricao: "Disciplina o funcionamento da seção de Arquivo e Depósito Judicial e dá outras providências.", 
            link: "file:///C:/Users/Mauricio/Downloads/portarias/arquivos/portarias2006/138.pdf" }
  ]
}